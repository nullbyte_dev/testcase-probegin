
from django.core.management.base import BaseCommand

from syncronizer.models import (
    Customer,
    CustomerDiscount,
)


class Command(BaseCommand):
    help = 'Management command to import all the data from old db into new one'

    def _syncronize(self, model, get_extra=None):
        sync_map = model.SYNC_MAP
        actual = model.objects.values_list('sync_id', flat=True)
        exclude_lookup = {
            '{}__in'.format(sync_map['sync_id']): list(actual)
        }
        legacy_data = model.SYNC_WITH.objects.exclude(**exclude_lookup).values()
        instances = [
            model(**{
                **{actual_field: legacy_row[legacy_field] for actual_field, legacy_field in sync_map.items()},
                **(get_extra and get_extra(legacy_row) or {})
            }) for legacy_row in legacy_data
        ]
        model.objects.bulk_create(instances)
        self.stdout.write(self.style.SUCCESS(f'Successfully imported {len(instances)} rows from {model.SYNC_WITH.__name__}'))

    def handle(self, *args, **options):
        self._syncronize(Customer)
        customers_mapping = dict(Customer.objects.values_list('sync_id', 'pk'))
        # join foreign keys
        getter = lambda x: dict(customer_id=customers_mapping[x['intcustomerid_id']])
        self._syncronize(CustomerDiscount, getter)
