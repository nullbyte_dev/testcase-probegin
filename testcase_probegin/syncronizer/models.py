""" Define tables in new database """
import datetime
import pytz

from django.db import models

from legacy import models as legacy_models


class BaseSync(models.Model):
    SYNC_WITH = None
    SYNC_MAP = {}
    sync_id = models.IntegerField(db_index=True, editable=False)

    class Meta:
        abstract = True

    @classmethod
    def sync_details(cls):
        """ Returns diff between legacy and actual DB rows, if synced returns 0 """
        return cls.objects.count() - cls.SYNC_WITH.objects.count()

    @classmethod
    def is_synced(cls):
        return not cls.sync_details()

    def save(self, *args, **kwargs):
        """ Will update old DB when data is added/updated to new DB """
        created = not self.pk
        params = {
            legacy_field: getattr(self, actual_field)
            for actual_field, legacy_field in self.SYNC_MAP.items()
        }
        if created:
            legacy_model_object = self.SYNC_WITH.objects.create(**params)
            self.sync_id = legacy_model_object.pk
        else:
            self.SYNC_WITH.objects.filter(**{self.SYNC_MAP['sync_id']: self.sync_id}).update(**params)
        return super().save(*args, **kwargs)


class Customer(BaseSync):
    SYNC_WITH = legacy_models.Customer
    SYNC_MAP = {
        'sync_id': 'lcustomer_id',
        'search_name': 'csearchname',
        'name': 'cname',
        'account_number': 'naccountnr',
        'email_sender': 'vemailsender',
    }
    search_name = models.CharField(max_length=10)
    name = models.TextField()
    account_number = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)
    email_sender = models.EmailField()

    def __str__(self):
        return f'{self.search_name} <{self.email_sender}>'


class CustomerDiscount(BaseSync):
    SYNC_WITH = legacy_models.CustomerDiscount
    SYNC_MAP = {
        'sync_id': 'intcustomerdiscountid',
        'description': 'chvdescription',
        'insert_date': 'dtminsertdate',
    }
    customer = models.ForeignKey(Customer, related_name='discounts', on_delete=models.CASCADE, null=True)
    description = models.CharField(max_length=50)
    insert_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        # can't use auto_add_now on `insert_date` because field value
        # must be evaluated before syncronization with legacy DB
        self.insert_date = datetime.datetime.now(tz=pytz.UTC)
        return super().save(*args, **kwargs)
