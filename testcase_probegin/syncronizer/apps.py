from django.apps import AppConfig


class SyncronizerConfig(AppConfig):
    name = 'syncronizer'
