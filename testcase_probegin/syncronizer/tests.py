from decimal import Decimal

from django.test import TestCase

from syncronizer import models as actual_models


class SyncMixin:
    multi_db = True
    actual_model = None
    legacy_model = None

    def test_create(self):
        params = self.get_params()
        actual_object = self.actual_model.objects.create(**params)
        self.assertEqual(
            self.actual_model.objects.count(),
            self.legacy_model.objects.count()
        )
        legacy_object = self.legacy_model.objects.get(pk=actual_object.sync_id)
        for actual_field, legacy_field in self.actual_model.SYNC_MAP.items():
            self.assertEqual(
                getattr(actual_object, actual_field),
                getattr(legacy_object, legacy_field),
            )

    def test_update(self):
        params = self.get_params()
        actual_object = self.actual_model.objects.create(**params)
        params = {
            k: f'{v}-edited' if isinstance(v, str) else v
            for k, v in params.items()
        }
        for k, v in params.items():
            setattr(actual_object, k, v)
        actual_object.save()
        legacy_object = self.legacy_model.objects.get(pk=actual_object.sync_id)
        for actual_field, legacy_field in self.actual_model.SYNC_MAP.items():
            self.assertEqual(
                getattr(actual_object, actual_field),
                getattr(legacy_object, legacy_field),
            )

    def get_params(self):
        return {}


class TestCustomer(SyncMixin, TestCase):
    actual_model = actual_models.Customer
    legacy_model = actual_model.SYNC_WITH

    def get_params(self):
        return {
            'search_name': 'test-search-name',
            'name': 'test-name',
            'account_number': Decimal(100),
            'email_sender': 'test@example.com',
        }


class TestCustomerDiscount(SyncMixin, TestCase):
    actual_model = actual_models.CustomerDiscount
    legacy_model = actual_model.SYNC_WITH

    def get_params(self):
        return {
            'description': 'test-description',
        }
