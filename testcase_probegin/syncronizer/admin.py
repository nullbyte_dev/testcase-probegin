""" Expose these tables via Admin interface, inlining Discounts for each customer """
from django.contrib import admin

from . import models


class CustomerDiscountInline(admin.TabularInline):
    model = models.CustomerDiscount
    fields = ('description', 'insert_date')
    readonly_fields = ('insert_date',)


@admin.register(models.Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('search_name', 'name', 'account_number', 'email_sender')
    inlines = [CustomerDiscountInline]


@admin.register(models.CustomerDiscount)
class CustomerDiscountAdmin(admin.ModelAdmin):
    list_display = ('customer', 'description', 'insert_date')
