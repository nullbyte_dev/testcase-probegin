from django.views.generic import TemplateView

from . import models


class Index(TemplateView):
    """ Show sync status & link to Admin panel """
    template_name = 'syncronizer/index.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data()
        tracked = (models.Customer, models.CustomerDiscount)
        synced = all(x.is_synced() for x in tracked)
        details = None
        if not synced:
            details = ((model.__name__, model.sync_details()) for model in tracked)
            details = [(name, diff, abs(diff)) for name, diff in details]
        ctx.update(synced=synced, details=details)
        return ctx
