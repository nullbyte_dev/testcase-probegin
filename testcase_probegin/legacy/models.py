# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Customer(models.Model):
    lcustomer_id = models.AutoField(db_column='lCustomer_id', primary_key=True)  # Field name made lowercase.
    csearchname = models.CharField(db_column='cSearchName', max_length=10)  # Field name made lowercase.
    cname = models.TextField(db_column='cName')  # Field name made lowercase.
    naccountnr = models.DecimalField(db_column='nAccountNr', max_digits=10, decimal_places=5, blank=True, null=True)  # Field name made lowercase. max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    vemailsender = models.TextField(db_column='vEmailSender', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        # managed = False
        db_table = 'CUSTOMER'


class CustomerDiscount(models.Model):
    intcustomerdiscountid = models.AutoField(db_column='intCustomerDiscountId', primary_key=True)  # Field name made lowercase.
    chvdescription = models.CharField(db_column='chvDescription', max_length=50, blank=True, null=True)  # Field name made lowercase.
    dtminsertdate = models.DateTimeField(db_column='dtmInsertDate')  # Field name made lowercase.
    intcustomerid = models.ForeignKey(Customer, models.DO_NOTHING, db_column='intCustomerId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        # managed = False
        db_table = 'CUSTOMER_DISCOUNT'
