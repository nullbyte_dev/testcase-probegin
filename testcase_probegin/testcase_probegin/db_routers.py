class SyncronizerRouter:
    LEGACY = 'legacy'

    def db_for_read(self, model, **hints):
        """ Reads from legacy models go to `legacy` DB """
        if model._meta.app_label == self.LEGACY:
            return self.LEGACY
        return None

    # same as writes
    db_for_write = db_for_read

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """ `legacy` DB does not take part in migrations """
        if app_label == self.LEGACY:
            return db == self.LEGACY
        elif db == self.LEGACY:
            return False
        return None
